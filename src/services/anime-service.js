const animeRepository = require('../repositories/anime-repository')

module.exports.create = async (anime) => {
    await animeRepository.save(anime)
}

module.exports.readAll = async () => {
    return await animeRepository.findAll()
}

module.exports.readOne = async (id) => {
    return await animeRepository.findOne(id)
}

module.exports.update = async (id, anime) => {
    return await animeRepository.update(id, anime)
}

module.exports.delete = async (id) => {
    return await animeRepository.delete(id)
}
