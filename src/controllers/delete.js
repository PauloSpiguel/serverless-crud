const animeService = require('../services/anime-service')

module.exports.handler = async (event) => {

    try {
        const id = event.pathParameters.id

        await animeService.delete(id)

        return {
            statusCode: 204
        }
    } catch (error) {
        console.error(error);

        return {
            statusCode: 500,
            body: JSON.stringify(`ERROR! Failed to delete: ${error}`)
        }
    }
}