const animeService = require('../services/anime-service')

module.exports.handler = async (event) => {

    try {
        const animes = await animeService.readAll()

        if (animes) {
            return {
                statusCode: 200,
                body: JSON.stringify(animes)
            }
        } else {
            return {
                statusCode: 404,
                body: JSON.stringify({
                    message: 'ERROR! Anime list is empty'
                })
            }
        }
    } catch (error) {
        console.error(error);

        return {
            statusCode: 500,
            body: JSON.stringify(`ERROR! Failed to list: ${error}`)
        }
    }
}