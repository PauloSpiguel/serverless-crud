const animeService = require('../services/anime-service')

module.exports.handler = async (event) => {

    try {
        const id = event.pathParameters.id
        const anime = JSON.parse(event.body)

        await animeService.update(id, anime)

        return {
            statusCode: 204
        }
    } catch (error) {
        console.error(error);

        return {
            statusCode: 500,
            body: JSON.stringify(`ERROR! Failed to update: ${error}`)
        }
    }
}