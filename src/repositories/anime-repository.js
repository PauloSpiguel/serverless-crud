const aws = require('aws-sdk')
const dynamoDb = new aws.DynamoDB.DocumentClient()
const TableName = process.env.ANIME_TABLE
const { v1: uuid } = require('uuid') 

module.exports.save = async (anime) => {
    const id = uuid()
    
    const params = {
        TableName,
        Item: {
            'id': id,
            'title': anime.title,
            'studio': anime.studio,
            'season': anime.season,
            'rating': anime.rating
        }
    }
    await dynamoDb.put(params).promise()
}

module.exports.findAll = async () => {
    const params = {
        TableName,
    }

    const animes = await dynamoDb.scan(params).promise()

    if (animes.Items) {
        return animes
    } else {
        return undefined
    }
}

module.exports.findOne = async (id) => {
    const params = {
        TableName,
        Key: {
            "id": id
        }
    }

    const anime = await dynamoDb.get(params).promise()

    if (anime.Item) {
        return anime
    } else {
        return undefined
    }
}

module.exports.update = async (id, anime) => {
    const params = {
        TableName,
        Key: {
            "id": id
        },
        ConditionExpression: "id = :id",
        UpdateExpression: "set title = :t, studio = :stu, season = :se, rating = :r",
        ExpressionAttributeValues: {
            ":id": id,
            ":t": anime.title,
            ":stu": anime.studio,
            ":se": anime.season,
            ":r": anime.rating
        },
        ReturnValues: "UPDATED_NEW"
    }

    return await dynamoDb.update(params).promise()
}

module.exports.delete = async (id) => {
    const params = {
        TableName,
        Key: {
            "id": id
        },
        ConditionExpression: "id = :id",
        ExpressionAttributeValues: {
            ":id": id
        }
    }

    return await dynamoDb.delete(params).promise()
}